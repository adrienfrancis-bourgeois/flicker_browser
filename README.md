# Instagrangeme

## Demo

The project is deployed at this address: http://instagrangeme.s3-website-ap-southeast-2.amazonaws.com/

## Disclaimer

I have lost the git history. To boostrap the project I did a git clone --depth=1 from this repo https://github.com/react-boilerplate/react-boilerplate and later on when I tried to push my repo I got the following error:

```
fatal: attempt to fetch/clone from a shallow repository
```

I couldn't find an easy fix for that one so had to reinit git on the project.

## Setup

```npm install```

with a recent version of node (I have used 8.15.1)

## Development

```npm start```

## test + coverage report

```npm test```

## build

```npm build```

## Known limitations

I have used css and javascript methods which are uncompatible with very old browsers.

## Challenge faced

I actually faced a pretty tough challenge when it came to query the flickr feed. Indeed neither the feed nor the apis allow cross origin sharing. Which means that any request sent from the browser was returning a CORS error making it impossible to implement the application as per specified in the challenge. A solution would have been to deploy a proxy and allow cross site sharing and make the requests to flickr from the proxy. It seemed obvious to me that this was out of scope.

After intense googling I found a loophole: jquery.getJSON with this exact URL "https://www.flickr.com/services/feeds/photos_public.gne?jsoncallback=?" (this bit is very important: "jsoncallback=?") doesn't trigger a CORS error. I didn't understand why exactly it is working but I would love to discuss it with you and get to know what the other candidates did.

This stackoverflow was the most helpful: https://stackoverflow.com/questions/47010723/flickr-api-call-with-xmlhttprequest-not-working

## TODOs

There are still a fair few things to do before this app is "production" ready. However I've already spent a significant amount of time working on it (way more than 3 hours :) ) so I am going to list them here:

- The test coverage is not a 100% (around 90 which is not too bad) and some components should be better tested.
- I have disabled eslint to be more efficient. So re-enable eslint and fix all the errors.
- We could certainly improve the error handling as atm we just display a generic error message.
- As I was running short on time I have allowed myself some inline CSS which I would never do for a production application.
- The components include "copies" which is not really good practice. Ideally all those copies should be extracted in a `message.js` file and use i18n. The copies would be a lot easier to update and manage and it would also be very easy to allow the copies to be translated (with i18n).
- There are potentially a lot of UXs improvements that could be done:
  - Flickr seems to always return stuff like this for the author: `nobody@flickr.com ("chrisgentlephoto")`, some smart parsing could improve this.
  - Sometimes there a lot of tags, we should only show 5 or so and if there are more put them into an expandable block
  - I have used quite a few material-ui components including the "Card" one that I thought was really well fitted for this challenge. Although it is not always doing a great job with the pictures. Indeed the component tries to fit the picture as well as it can in the card so we have nice cards with pictures having the same size. That's great from an UI/UX point of view however pictures on flickr come in a lot of different sizes and proportions which doesn't always play well with the "Card" component.
  - We could allow the tags underneath the pictures to be clicked and to be automatically added to the search.
  - We could add a tickbox to allow users to search for "every" or "any" tags (it is currently searching for every tags only)
  - We could add a dropdown allowing users to search for content in other languages (it is currently searching for content in english only).

## Implementation details

- This project uses React with Redux
- I have used react-boilerplate (https://github.com/react-boilerplate/react-boilerplate) to bootstrap the project.
- Most of the UI components are coming from material-ui (https://material-ui.com)