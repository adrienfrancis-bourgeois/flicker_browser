import jquery from 'jquery';

export default function queryFlickr(tags) {
  const foobar = jquery;
  let promise = new Promise((resolve, reject) => {
    jquery.getJSON(
      'https://www.flickr.com/services/feeds/photos_public.gne?jsoncallback=?',
      {
        tags: tags.map(e => e.value).join(','),
        tagmode: "all",
        format: "json",
        lang: 'en-us',
      }
    ).done( data => {
      resolve(data.items);
    }).fail( error => {
      reject(error);
    });
  });  
  return promise;
};
