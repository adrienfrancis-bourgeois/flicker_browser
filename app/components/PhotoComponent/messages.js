/*
 * PhotoComponent Messages
 *
 * This contains all the text for the PhotoComponent component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.PhotoComponent';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the PhotoComponent component!',
  },
});
