/**
 *
 * PhotoComponent
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import LabelIcon from '@material-ui/icons/Label';

import { FormattedDate } from 'react-intl';
import messages from './messages';



import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  chip: {
    margin: theme.spacing(0.2),
    background: '#e0e0e0',
    border: '0px',
  },
  card: {
    maxWidth: 345,
    margin: 'auto',
  },
}));

function PhotoComponent(props) {
  const { photoItem } = props;
  const classes = useStyles();

  return(
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt={photoItem.title}
          height="200"
          image={photoItem.media.m}
          title={photoItem.title}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {photoItem.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {photoItem.tags.split(' ').map(tag =>
              <Chip
                icon={<LabelIcon />}          
                label={tag}
                variant="outlined"
                className={classes.chip}
                size="small"
                key={tag}
              />
            )}
          </Typography>
          <br />
          <Typography variant="body2" color="textSecondary" component="p">
            Photo by {photoItem.author}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            published on&nbsp;
            <FormattedDate
              value={photoItem.published}
              day="numeric"
              month="long"
              year="numeric"
            />
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" href={photoItem.link} target="_blank">
          view on flickr
        </Button>
      </CardActions>
    </Card>
  );

}

PhotoComponent.propTypes = {
  photoItem: PropTypes.object,
};

export default PhotoComponent;
