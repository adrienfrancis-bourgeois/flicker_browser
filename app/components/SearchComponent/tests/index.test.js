/**
 *
 * Tests for SearchComponent
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
// import 'jest-dom/extend-expect'; // add some helpful assertions

import SearchComponent from '../index';
import { DEFAULT_LOCALE } from '../../../i18n';

describe('<SearchComponent />', () => {
  it('Expect to not log errors in console', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <SearchComponent />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });
});
