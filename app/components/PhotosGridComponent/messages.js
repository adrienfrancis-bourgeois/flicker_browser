/*
 * PhotosGridComponent Messages
 *
 * This contains all the text for the PhotosGridComponent component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.PhotosGridComponent';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the PhotosGridComponent component!',
  },
});
