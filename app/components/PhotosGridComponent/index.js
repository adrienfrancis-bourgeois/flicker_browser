/**
 *
 * PhotosGridComponent
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import CameraIcon from '@material-ui/icons/Camera';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { makeStyles } from '@material-ui/core/styles';
import PhotoComponent from '../PhotoComponent/index';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    color: '#666',
  },
}));

function PhotosGridComponent(props) {
  const { photoItems } = props;
  const classes = useStyles();

  if (photoItems === null) {
    return (
      <div data-testid="PhotosGridComponent">
        <Paper className={classes.root}>
          <Typography variant="h5" component="h3" data-testid="startSearch">
            To start, enter a few tags in the search bar.
          </Typography>
          <Typography component="p">
            You can separate each of your keywords with a space and they will 
            automatically get converted to tags.
          </Typography>
        </Paper>
      </div>
    );
  }

  if (photoItems.length > 0) {
    return (
      <Grid
        container
        spacing={3}
        direction="row"
        justify="space-evenly"
        data-testid="PhotosGridComponent"
        >
        {photoItems.map(item =>
          <Grid item xs={12} sm={6} md={4} lg={3} key={item.media.m}>
            <PhotoComponent photoItem={item} />
          </Grid>
       )}
      </Grid>
    );
  }

  return (
    <div data-testid="PhotosGridComponent">
      <Paper className={classes.root}>
        <Typography variant="h5" component="h3" data-testid="startSearch">
          No results found
        </Typography>
        <Typography component="p">
          
        </Typography>
      </Paper>
    </div>
  );
}

PhotosGridComponent.propTypes = {
  photoItems: PropTypes.arrayOf(PropTypes.object),
};

export default PhotosGridComponent;
