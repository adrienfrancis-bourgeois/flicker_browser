/**
 *
 * Tests for PhotosGridComponent
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render } from '@testing-library/react';
import PhotosGridComponent from '../index';

jest.mock('../../PhotoComponent/index', () => () => {
  return <div data-testid="PhotoComponent"></div>;
});

describe('<PhotosGridComponent />', () => {
  describe('when there is at least one photo item', () => {
    it('shows PhotoComponent', () => {
      const { getAllByTestId } = render(
        <PhotosGridComponent
          photoItems={[
            { media: { m: '1' } },
            { media: { m: '2' } },
            { media: { m: '3' } },
          ]}
        />
      );
      expect(getAllByTestId('PhotoComponent').length).toBe(3);
    });
  });

  describe('when there is no photo item', () => {
    it('shows a message asking the user to start a search', () => {
      const { getAllByTestId } = render(
        <PhotosGridComponent photoItems={[]} />
      );
      expect(getAllByTestId('startSearch').length).toBe(1);
    });
  });
});
