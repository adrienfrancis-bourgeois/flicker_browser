/*
 * LoadingComponent Messages
 *
 * This contains all the text for the LoadingComponent component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.LoadingComponent';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the LoadingComponent component!',
  },
});
