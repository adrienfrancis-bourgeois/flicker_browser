/**
 *
 * LoadingComponent
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';
import { makeStyles } from '@material-ui/core/styles';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const useStyles = makeStyles(theme => ({
  progress: {
    margin: theme.spacing(2),
  },
}));

function LoadingComponent(props) {
  const { progress, children } = props;
  const classes = useStyles();

  if (progress) {
    return (
      <div data-testid="LoadingComponent">
        <LinearProgress
          className={classes.progress}
          data-testid="LinearProgress"
        />
      </div>
    );
  } else {
    return <div data-testid="LoadingComponent">{children}</div>;
  }
}

LoadingComponent.propTypes = {
  progress: PropTypes.bool,
};

export default LoadingComponent;
