/**
 *
 * Tests for LoadingComponent
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render } from '@testing-library/react';
import LoadingComponent from '../index';

describe('<LoadingComponent />', () => {
  describe('when progress is true', () => {
    it('shows the loading screen', () => {
      const { getAllByTestId } = render(
        <LoadingComponent progress={true}>
          <div id="childComponent"></div>
        </LoadingComponent>
      );
      expect(getAllByTestId('LinearProgress').length).toBe(1);
    });
  });

  describe('when progress is false', () => {
    it('shows the children', () => {
      const { getAllByText } = render(
        <LoadingComponent progress={false}>
          <div>child content</div>
        </LoadingComponent>
      );
      expect(getAllByText('child content').length).toBe(1);
    });
  });
});
