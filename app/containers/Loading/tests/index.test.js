/**
 *
 * Tests for Loading
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';

import { queryPhotos } from '../../Search/actions';
import Loading from '../index';
import configureStore from '../../../configureStore';

describe('<Loading />', () => {
  let store;
  const dispatch = jest.fn();

  beforeAll(() => {
    store = configureStore({});
  });

  it('contains a Loading component', () => {
    store.dispatch(queryPhotos('photos'));
    const { getAllByTestId } = render(
      <Provider store={store}>
        <Loading dispatch={dispatch} />
      </Provider>,
    );
    expect(getAllByTestId('LoadingComponent').length).toEqual(1);
  });
});
