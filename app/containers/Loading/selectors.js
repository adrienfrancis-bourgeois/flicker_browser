import { createSelector } from 'reselect';
import { initialState as searchInitialState } from '../Search/reducer';

/**
 * Direct selector to the loading state domain
 */

const selectSearchDomain = state => state.search || searchInitialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Loading
 */

const makeSelectLoading = () =>
  createSelector(
    selectSearchDomain,
    substate => substate.loading,
  );

export default makeSelectLoading;
export { selectSearchDomain };
