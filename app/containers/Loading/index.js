/**
 *
 * Loading
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import makeSelectLoading from './selectors';
import LoadingComponent from '../../components/LoadingComponent/index';

export function Loading(props) {
  const { loading, children } = props;
  return <LoadingComponent progress={loading}>{children}</LoadingComponent>;
}

Loading.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
});

const withConnect = connect(mapStateToProps);

export default compose(withConnect)(Loading);
