/*
 *
 * Search actions
 *
 */

import {
  UPDATE_TAGS,
  QUERY_PHOTOS,
  UPDATE_PHOTOS,
  UPDATE_ERROR,
} from './constants';

export function updateTags(tags) {
  let tagList = [];
  if (tags) {
    tags.forEach(tag => {
      tag.value.split(' ').forEach(e => {
        tagList.push({
          label: e,
          value: e,
          __isNew__: true,
        })
      })
    });
  }
  return {
    type: UPDATE_TAGS,
    tags: tagList,
  }
}

export function queryPhotos(tags) {
  return {
    type: QUERY_PHOTOS,
    tags,
  }
}

export function updatePhotos(photoItems) {
  return {
    type: UPDATE_PHOTOS,
    photoItems,
  }
}

export function updateError(error) {
  return {
    type: UPDATE_ERROR,
    error,
  }
}
