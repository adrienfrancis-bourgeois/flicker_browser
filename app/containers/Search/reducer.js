/*
 *
 * Search reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, UPDATE_TAGS, UPDATE_PHOTOS, UPDATE_ERROR } from './constants';

export const initialState = { photoItems: null };

/* eslint-disable default-case, no-param-reassign */
const searchReducer = (state = initialState, action) =>
  produce(state, (/* draft */) => {
    switch (action.type) {
      case UPDATE_TAGS:
        return { ...state, tags: action.tags, loading: true };
      case UPDATE_PHOTOS:
        return { ...state, photoItems: action.photoItems, loading: false, error: null };
      case UPDATE_ERROR:
        return { ...state, error: action.error, loading: false };
      default:
        return state;
    }
  });

export default searchReducer;
