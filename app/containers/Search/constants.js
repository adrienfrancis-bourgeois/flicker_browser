/*
 *
 * Search constants
 *
 */

export const UPDATE_TAGS = 'app/Search/UPDATE_TAGS';
export const QUERY_PHOTOS = 'app/Search/QUERY_PHOTOS';
export const UPDATE_PHOTOS = 'app/Search/UPDATE_PHOTOS';
export const UPDATE_ERROR = 'app/Search/UPDATE_ERROR';
