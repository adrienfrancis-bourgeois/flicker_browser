import { takeLatest, call, put, select } from 'redux-saga/effects';
import { QUERY_PHOTOS } from './constants';
import { updateTags, updatePhotos, updateError } from './actions';
import queryFlickr from '../../utils/queryFlickr';

export const getTags = state =>
  state.search.tags

export function* queryPhotos(action) {
  yield put(updateTags(action.tags));
  // yield put(updateError(null));
  const tags = yield select(getTags);
  if (tags && tags.length > 0) {
    try {
      const results = yield call(
        queryFlickr,
        tags,
      );
      yield put(updatePhotos(results));
    } catch (e) {
      yield put(updateError("Something went wrong while querying flickr"));
    }
  } else {
    yield put(updatePhotos(null));
  }
}

export default function* searchSaga() {
  yield takeLatest(QUERY_PHOTOS, queryPhotos);
}
