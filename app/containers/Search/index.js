/**
 *
 * Search
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectSearch from './selectors';
import reducer from './reducer';
import saga from './saga';
import { queryPhotos } from './actions';
import SearchComponent from '../../components/SearchComponent/index';

export function Search(props) {
  useInjectReducer({ key: 'search', reducer });
  useInjectSaga({ key: 'search', saga });

  const { onUpdateTags } = props;
  const { tags, error } = props.search;

  return <SearchComponent
    tags={tags}
    error={error}
    onUpdateTags={onUpdateTags}
  />;
}

Search.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  search: makeSelectSearch(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onUpdateTags: event => {
      dispatch(queryPhotos(event));
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Search);
