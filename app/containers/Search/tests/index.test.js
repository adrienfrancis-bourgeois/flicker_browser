/**
 *
 * Tests for Search
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { Provider } from 'react-redux';
// import { mount } from 'enzyme';
import { render } from '@testing-library/react';

import { updateTags, queryPhotos } from '../actions';
import Search, { mapDispatchToProps } from '../index';
import configureStore from '../../../configureStore';

describe('<Search />', () => {
  let store;
  const dispatch = jest.fn();
  const tags = [{ label: 'tag1', value: 'tag1' }];

  beforeAll(() => {
    store = configureStore({});
  });

  it('contains a SearchComponent component', () => {
    store.dispatch(updateTags(tags));
    const { getAllByTestId } = render(
      <Provider store={store}>
        <Search dispatch={dispatch} />
      </Provider>,
    );
    expect(getAllByTestId('SearchComponent').length).toEqual(1);
  });
});

describe('Search mapDispatchToProps', () => {
  const dispatch = jest.fn();
  let event = { target: { value: 'foobar' } };

  describe('onUpdateTags', () => {
    it('dispatches a queryPhotos with the event', () => {
      mapDispatchToProps(dispatch).onUpdateTags(event);
      expect(dispatch).toHaveBeenCalledWith(
        queryPhotos(event),
      );
    });
  });
});
