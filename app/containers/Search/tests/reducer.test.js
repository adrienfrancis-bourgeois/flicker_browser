// import produce from 'immer';
import searchReducer, { initialState } from '../reducer';
import { updateTags, updatePhotos, updateError } from '../actions';

/* eslint-disable default-case, no-param-reassign */
describe('searchReducer', () => {
  let state;
  beforeEach(() => {
    state = {
      // default state params here
    };
  });

  it('default: returns the initial state', () => {
    const expectedResult = initialState;
    expect(searchReducer(undefined, {})).toEqual(expectedResult);
  });

  it('updateTags', () => {
    const tags = [{ label: 'tag1', value: 'tag1' }];
    const expectedResult = {
      loading: true,
      photoItems: null,
      tags: [{ label: 'tag1', value: 'tag1', __isNew__: true }],
    };
    expect(searchReducer(undefined, updateTags(tags))).toEqual(expectedResult);
  });

  it('updatePhotos', () => {
    const photos = ['photos'];
    const expectedResult = {
      loading: false,
      photoItems: photos,
      error: null,
    };
    expect(searchReducer(undefined, updatePhotos(photos))).toEqual(expectedResult);
  });

  it('updateError', () => {
    const error = 'error';
    const expectedResult = {
      photoItems: null,
      loading: false,
      error,
    };
    expect(searchReducer(undefined, updateError(error))).toEqual(expectedResult);
  });
});
