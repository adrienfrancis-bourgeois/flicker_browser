import { updateTags, queryPhotos, updatePhotos, updateError } from '../actions';
import {
  UPDATE_TAGS,
  QUERY_PHOTOS,
  UPDATE_PHOTOS,
  UPDATE_ERROR,
} from '../constants';

describe('Search actions', () => {
  it('updateTags', () => {
    const tag1 = { label: 'tag1', value: 'tag1' };
    const tag2 = { label: 'tag2', value: 'tag2' };
    const tags = [tag1, tag2];
    const expected = {
      type: UPDATE_TAGS,
      tags: [
        {
          label: tag1.label,
          value: tag1.value,
          __isNew__: true,
        },
        {
          label: tag2.label,
          value: tag2.value,
          __isNew__: true,
        },
      ],
    };
    expect(updateTags(tags)).toEqual(expected);
  });

  it('updateTags when tags is null', () => {
    const tags = null;
    const expected = {
      type: UPDATE_TAGS,
      tags: [],
    };
    expect(updateTags(tags)).toEqual(expected);
  });

  it('queryPhotos', () => {
    const tags = 'tags';
    const expected = {
      type: QUERY_PHOTOS,
      tags,
    };
    expect(queryPhotos(tags)).toEqual(expected);
  });

  it('updatePhotos', () => {
    const photoItems = 'photoItems';
    const expected = {
      type: UPDATE_PHOTOS,
      photoItems,
    };
    expect(updatePhotos(photoItems)).toEqual(expected);
  });

  it('updateError', () => {
    const error = 'error';
    const expected = {
      type: UPDATE_ERROR,
      error,
    };
    expect(updateError(error)).toEqual(expected);
  });
});
