/**
 * Test sagas
 */

/* eslint-disable redux-saga/yield-effects */

import { call, put, takeLatest, select } from 'redux-saga/effects';
import searchSaga, { queryPhotos, getTags } from '../saga';
import { updateTags, updatePhotos, updateError } from '../actions';
import { QUERY_PHOTOS } from '../constants';
import queryFlickr from '../../../utils/queryFlickr';

describe('searchSaga Saga', () => {
  const saga = searchSaga();
  it('starts task to watch for QUERY_PHOTOS action', () => {
    const descriptor = saga.next().value;
    expect(descriptor).toEqual(
      takeLatest(QUERY_PHOTOS, queryPhotos),
    );
  });
});

describe('getTags', () => {
  it('gets the tags from the state', () => {
    const tags = 'tags';
    const state = {
      search: {
        tags: tags
      }
    };
    expect(getTags(state)).toEqual(tags);
  });
});

describe('queryPhotos', () => {
  let generator;
  const tags = [{ label: 'tag1', value: 'tag1' }];
  const action = { tags };
  beforeEach(() => {
    generator = queryPhotos(action);
  });

  it('dispatches an updateTags events', () => {
    const descriptor = generator.next().value;
    expect(descriptor).toEqual(put(updateTags(tags)));
  });

  it('gets the tags from the store', () => {
    generator.next();
    const descriptor = generator.next().value;
    expect(descriptor).toEqual(select(getTags));
  });

  describe('when there is no tags in the store', () => {
    it('update the photos in the store with an empty array', () => {
      generator.next();
      generator.next();
      const descriptor = generator.next(null).value;
      expect(descriptor).toEqual(put(updatePhotos(null)));
    })
  })

  describe('when there is at least one tag in the store', () => {
    it('calls the flickr api', () => {
      generator.next();
      generator.next();
      const descriptor = generator.next(tags).value;
      expect(descriptor).toEqual(call(queryFlickr, tags));
    });

    it('updates the photos in the store with the API response', () => {
      const results = 'results';
      generator.next();
      generator.next();
      generator.next(tags);
      const descriptor = generator.next(results).value;
      expect(descriptor).toEqual(put(updatePhotos(results)));
    });

    describe('when the api call returns an error', () => {
      it('updates the error msg in the store', () => {
        const results = 'results';
        generator.next();
        // generator.next();
        generator.next();
        generator.next(tags);
        const descriptor = generator.throw('error').value;
        expect(descriptor).toEqual(
          put(updateError("Something went wrong while requesting flickr")),
        );
      });
    });
  });
});
