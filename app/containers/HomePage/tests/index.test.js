/**
 *
 * Tests for HomePage
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';

import { queryPhotos } from '../../Search/actions';
import HomePage from '../index';
import configureStore from '../../../configureStore';

describe('<HomePage />', () => {
  let store;
  const dispatch = jest.fn();

  beforeAll(() => {
    store = configureStore({});
  });

  it('contains a Loading and PhotosGrid component', () => {
    const { getAllByTestId } = render(
      <Provider store={store}>
        <HomePage dispatch={dispatch} />
      </Provider>,
    );
    expect(getAllByTestId('LoadingComponent').length).toEqual(1);
    expect(getAllByTestId('PhotosGridComponent').length).toEqual(1);
  });
});
