/**
 *
 * HomePage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Container from '@material-ui/core/Container';

import messages from './messages';
import Search from '../Search/index';
import PhotosGrid from '../PhotosGrid/index';
import Loading from '../Loading/index';

export function HomePage(props) {

  const { dispatch } = props;
  return (
    <div>
      <div style={{minHeight: '90vh'}}>
        <div style={{
          textAlign: 'center',
          backgroundColor: '#4266b2',
          paddingBottom: '3vw',
          paddingLeft: 20,
          paddingRight: 20,
          color: 'white',
        }}>
          <div style={{
            fontFamily: "'Cookie',cursive",
            fontSize: '12vw',            
          }}>
            Instagrangeme
          </div>
          <Search dispatch={dispatch} />
        </div>
        <br />
        <Container maxWidth="lg">
          <Loading dispatch={dispatch}>
            <PhotosGrid dispatch={dispatch} />
          </Loading>
        </Container>
      </div>
      <div>
        <div style={{
          fontFamily: "'Cookie',cursive",
          fontSize: '30px',
          backgroundColor: '#333',
          paddingBottom: '3vw',
          paddingTop: 40,
          paddingBottom: 40,
          paddingRight: 20,
          marginTop: 30,
          color: 'white',
          textAlign: 'right',
        }}>
          Adrien Bourgeois
        </div>
      </div>
    </div>
  );
}

HomePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(HomePage);
