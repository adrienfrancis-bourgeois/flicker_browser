/**
 *
 * PhotosGrid
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import makeSelectPhotosGrid from './selectors';
import PhotosGridComponent from '../../components/PhotosGridComponent/index';

export function PhotosGrid(props) {
  const { photoItems } = props;

  return <PhotosGridComponent photoItems={photoItems} />;
}

PhotosGrid.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  photoItems: makeSelectPhotosGrid(),
});

const withConnect = connect(mapStateToProps);

export default compose(withConnect)(PhotosGrid);
