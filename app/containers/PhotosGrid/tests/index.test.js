/**
 *
 * Tests for PhotosGrid
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';

import { queryPhotos } from '../../Search/actions';
import PhotosGrid from '../index';
import configureStore from '../../../configureStore';

describe('<PhotosGrid />', () => {
  let store;
  const dispatch = jest.fn();

  beforeAll(() => {
    store = configureStore({});
  });

  it('contains a PhotosGrid component', () => {
    const { getAllByTestId } = render(
      <Provider store={store}>
        <PhotosGrid dispatch={dispatch} />
      </Provider>,
    );
    expect(getAllByTestId('PhotosGridComponent').length).toEqual(1);
  });
});
