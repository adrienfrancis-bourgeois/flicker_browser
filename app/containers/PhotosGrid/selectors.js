import { createSelector } from 'reselect';
import { initialState as searchInitialState } from '../Search/reducer';

/**
 * Direct selector to the photosGrid state domain
 */

const selectSearchDomain = state => state.search || searchInitialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by PhotosGrid
 */

const makeSelectPhotosGrid = () =>
  createSelector(
    selectSearchDomain,
    substate => substate.photoItems,
  );

export default makeSelectPhotosGrid;
export { selectSearchDomain };
